import styled, { css } from 'styled-components';

export const Container = styled.div`
  padding-bottom: 40px;
  overflow-y: scroll;
  height: 100%;

  ::-webkit-scrollbar {
    width: 5px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: #f1f1f1;
    border-radius: 10px;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: #888;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #555;
  }
`;

export const Grid = styled.div`
  ${({ theme }) => css`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-row-gap: ${theme.spacing.sm};
  `};
`;

export const ButtonWrapper = styled.div`
  ${() => css`
    width: 100%;
    display: flex;
    justify-content: center;
    margin-top: 50px;
  `};
`;
