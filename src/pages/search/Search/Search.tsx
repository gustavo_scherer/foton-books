import * as React from 'react';
import { useSearchResult } from 'useCases/contexts/search';
import { parseAuthor } from 'lib/contexts/book';
import { Book, Button } from 'components/structure';
import { BookSkeleton } from 'components/structure/Book/Book.skeleton';
import { numberOfBooksPerPage } from 'constants/books';
import * as S from './Search.styles';

export const Search = () => {
  const { data, isFetching, fetchNextPage, hasNextPage } = useSearchResult();

  return (
    <S.Container>
      <S.Grid>
        {data?.pages.map((page, index) => (
          <React.Fragment key={index}>
            {page &&
              page.map((item) => (
                <Book
                  key={item.id}
                  coverSrc={item.volumeInfo.imageLinks?.smallThumbnail}
                  title={item.volumeInfo.title}
                  author={parseAuthor(item.volumeInfo.authors)}
                  bookId={item.id}
                />
              ))}
          </React.Fragment>
        ))}
        {isFetching &&
          Array.from(Array(numberOfBooksPerPage).keys()).map((key) => (
            <BookSkeleton key={key} />
          ))}
      </S.Grid>
      <S.ButtonWrapper>
        <Button
          onClick={() => fetchNextPage()}
          disabled={isFetching || !hasNextPage}
        >
          {hasNextPage
            ? isFetching
              ? 'Loading more...'
              : 'Load More'
            : 'No more items'}
        </Button>
      </S.ButtonWrapper>
    </S.Container>
  );
};
