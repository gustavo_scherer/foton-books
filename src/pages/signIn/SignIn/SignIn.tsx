import { useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { useAuthContext } from 'contexts';
import { TextField } from 'components/form';
import { Button, Icon, Stack } from 'components/structure';
import { ROUTES } from 'constants/urls';
import * as S from './SignIn.styles';

export const SignIn = () => {
  const [inputValue, setInputValue] = useState('');

  const navigate = useNavigate();
  const { signIn, signed } = useAuthContext();

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setInputValue(e.target.value);
  };

  const handleFormSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    signIn(inputValue);
    navigate(ROUTES.app.getLink('home'));
  };

  if (signed) {
    return <Navigate to={ROUTES.app.getLink('home')} />;
  }

  return (
    <S.Container>
      <S.Wrapper>
        <S.LogoWrapper>
          <Icon icon="IcLogo" width="240px" height="80px" />
        </S.LogoWrapper>
        <S.Form onSubmit={handleFormSubmit}>
          <Stack spacing="10px">
            <TextField
              label="Name"
              placeholder="Insert your name to enter the app"
              onChange={handleInputChange}
            />
            <Button type="submit">Enter app</Button>
          </Stack>
        </S.Form>
      </S.Wrapper>
    </S.Container>
  );
};
