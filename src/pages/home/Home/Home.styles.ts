import styled, { css } from 'styled-components';
import media from 'styled-media-query';
import ovalUrl from 'assets/icons/core/oval.svg';

type SectionProps = {
  withBackground?: boolean;
  mobileAllRightSpace?: boolean;
};

type ReviewProps = {
  imageSrc: string;
};

const sectionModifiers = {
  withBackground: () => css`
    background-image: url(${ovalUrl});
    background-repeat: no-repeat;
    background-position: 110% 0%;
  `,
  mobileAllRightSpace: () => css`
    margin-right: -20px;

    & > div > ${Link} {
      margin-right: 15px;
    }
  `,
};

const reviewModifiers = {
  image: (src: string) => css`
    background-image: url(${src});
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    border-radius: 5px;
  `,
};

export const Container = styled.div``;

export const Greetings = styled.div`
  ${({ theme }) => css`
    color: ${theme.colors.grayscale.light.name};
    font-weight: 400;
    font-size: 24px;
    opacity: 0.6;
  `}
`;

export const Name = styled.span`
  ${({ theme }) => css`
    color: ${theme.colors.primary.main};
    font-weight: 600;
    margin-right: 11px;
  `}
`;

export const Section = styled.section<SectionProps>`
  ${({ withBackground = false, mobileAllRightSpace = false }) => css`
    .slick-center {
      transform: scale(1.08);
    }

    ${media.lessThan('medium')`
      ${mobileAllRightSpace && sectionModifiers.mobileAllRightSpace()};
    `}

    ${withBackground && sectionModifiers.withBackground()};
  `}
`;

export const Link = styled.a`
  ${({ theme }) => css`
    color: ${theme.colors.link.main};
    font-weight: 500;
    font-size: 14px;
    cursor: pointer;

    &:hover {
      text-decoration: underline;
    }
  `}
`;

export const SectionHeader = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const SectionTitle = styled.section`
  ${({ theme }) => css`
    color: ${theme.colors.grayscale.light.section};
    font-size: 18px;
    font-weight: 500;
    letter-spacing: 0.5px;
    margin-bottom: 15px;
  `}
`;

export const Review = styled.div<ReviewProps>`
  ${({ imageSrc }) => css`
    height: 300px;
    width: 100%;
    cursor: pointer;
    transition: all 0.2s linear;

    &:hover {
      opacity: 0.6;
    }

    ${imageSrc && reviewModifiers.image(imageSrc)};
  `};
`;

export const ReviewLink = styled.a``;
