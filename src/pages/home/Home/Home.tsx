import Slider from 'react-slick';
import { useBooksByIds, useBookById } from 'useCases/contexts/home';
import { isEven } from 'lib/contexts/home';
import { useAuthContext } from 'contexts';
import { CurrentlyReadingCard, DiscoverCard } from 'components/contexts/home';
import { Stack } from 'components/structure';
import { discoverNewBooksIds, currentlyReadingBook } from 'constants/books';
import videoThumbUrl from 'assets/images/video-thumb.png';
import * as S from './Home.styles';

const settings = {
  arrows: false,
  className: 'center',
  dots: false,
  infinite: false,
  centerMode: true,
  centerPadding: '30px 0 5px',
  speed: 500,
  slidesToShow: 1.2,
  slidesToScroll: 1,
  initialSlide: 0,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        centerPadding: '3px 0 0',
      },
    },
  ],
};

export const Home = () => {
  const { username } = useAuthContext();
  const discoverNewBooksQuery = useBooksByIds(discoverNewBooksIds);
  const { data, isLoading } = useBookById(currentlyReadingBook);

  return (
    <S.Container>
      <Stack spacing="30px">
        <S.Greetings>
          Hi, <S.Name>{username}</S.Name> 👋{' '}
        </S.Greetings>
        <S.Section withBackground mobileAllRightSpace>
          <S.SectionHeader>
            <S.SectionTitle>Discover new book</S.SectionTitle>
            <S.Link>More</S.Link>
          </S.SectionHeader>
          <Slider {...settings}>
            {discoverNewBooksQuery.map((book, index) => (
              <DiscoverCard
                key={`${data?.data.volumeInfo.title}-${index}`}
                backgroundPosition={isEven(index) ? 'top' : 'bottom'}
                isPurple={!isEven(index)}
                book={book.data?.data}
                isLoading={book.isLoading}
              />
            ))}
          </Slider>
        </S.Section>
        <S.Section>
          <S.SectionHeader>
            <S.SectionTitle>Currently Reading</S.SectionTitle>
            <S.Link>All</S.Link>
          </S.SectionHeader>
          <CurrentlyReadingCard book={data?.data} isLoading={isLoading} />
        </S.Section>
        <S.Section>
          <S.SectionHeader>
            <S.SectionTitle>Reviews of the Days</S.SectionTitle>
            <S.Link>All Video</S.Link>
          </S.SectionHeader>
          <S.ReviewLink
            href="https://www.youtube.com/watch?v=vBzBgewl4ac"
            target="_blank"
            rel="noreferrer"
          >
            <S.Review imageSrc={videoThumbUrl} />
          </S.ReviewLink>
        </S.Section>
      </Stack>
    </S.Container>
  );
};
