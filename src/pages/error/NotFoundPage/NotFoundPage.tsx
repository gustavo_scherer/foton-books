import { ErrorGeneric } from 'components/structure';

export const NotFound = () => (
  <ErrorGeneric
    title="Page not found..."
    description="Sorry, but the page you are looking for was not found. Please make sure you have entered the correct URL. 404 error."
    buttonLabel="Go Home"
    actionNavigate="/home"
  />
);
