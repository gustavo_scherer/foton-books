import styled, { css } from 'styled-components';
import media from 'styled-media-query';
import { Icon } from 'components/structure';
import bookBackground from 'assets/icons/core/book-background.svg';
import ovalUrl from 'assets/icons/core/oval.svg';

export const Container = styled.div`
  width: 100%;
  height: 100%;
  min-height: 100vh;
  overflow: hidden;
  background-color: #fffcf9;
  padding-bottom: 60px;
`;

export const Wrapper = styled.div`
  max-width: 1120px;
  width: 100%;
  margin: 0 auto;

  ${media.lessThan('medium')`
    padding: 0 30px;
  `}
`;

export const BackButton = styled.button`
  transition: all 0.2s linear;

  &:hover {
    opacity: 0.6;
  }

  > ${Icon} {
    width: 14px;
    height: 14px;
    margin-top: 55px;
  }
`;

export const Header = styled.div`
  ${({ theme }) => css`
    width: 100%;
    height: 282px;
    background-color: ${theme.colors.background.book.header};
    border-bottom-right-radius: 100px;
    position: relative;
  `}
`;

export const HeaderBackground = styled.div`
  position: absolute;
  top: -45px;
  right: -20px;
  height: 200px;
  width: 200px;
  transform: rotate(35deg);
  background-image: url(${ovalUrl});
  background-size: 120px;
  background-repeat: no-repeat;
  background-position: 102% -25%;
`;

export const ImageContainer = styled.div`
  position: absolute;
  bottom: -20px;
  left: 50%;
  transform: translateX(-50%);
  width: 240px;
  height: 229px;
  background-image: url(${bookBackground});
  background-repeat: no-repeat;
  background-position: center;
  display: flex;
  justify-content: center;

  &::after {
    content: '';
    display: block;
    position: absolute;
    width: 9.375rem;
    margin: 0px auto;
    height: 1rem;
    bottom: -27px;
    left: 0;
    right: 0;
    background: rgb(216, 216, 216);
    filter: blur(0.6875rem);
  }
`;

export const BookCover = styled.img`
  width: 151px;
  height: 234px;
  border-radius: 4px;
`;

export const Content = styled.div`
  max-width: 1120px;
  width: 100%;
  padding: 70px 20px;
  margin: 0 auto;
`;

export const Title = styled.h1`
  ${({ theme }) => css`
    color: ${theme.colors.grayscale.light.title};
    font-size: 24px;
  `}
`;

export const Author = styled.span`
  ${({ theme }) => css`
    color: ${theme.colors.primary.main};
    font-size: 16px;
    font-weight: 500;
  `}
`;

export const Summary = styled.div`
  ${({ theme }) => css`
    font-size: 14px;
    color: ${theme.colors.grayscale.light.paragraph};
    margin-top: 20px;
  `}
`;

export const ActionsMenu = styled.div`
  ${({ theme }) => css`
    display: grid;
    max-width: 335px;
    width: 100%;
    grid-template-columns: 1fr 1fr 1fr;
    background-color: ${theme.colors.neutral.white};
    position: fixed;
    z-index: ${theme.layers.alwaysOnTop};
    bottom: 3.125rem;
    left: 50%;
    transform: translateX(-50%);
    height: 56px;
    box-shadow: rgb(107 103 70 / 13%) 3px 3px 23px 0px;
  `}
`;

export const Option = styled.a`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: transparent;
    border: 0px;
    position: relative;
    font-weight: 700;
    transition: all 0.2s linear;
    cursor: pointer;
    color: ${theme.colors.neutral.black};

    &:hover {
      opacity: 0.6;
    }

    & > ${Icon} {
      width: 16px;
      height: 16px;
      margin-right: 10px;
    }

    &:not(:first-child) {
      &::before {
        content: '';
        display: block;
        width: 1px;
        height: 30%;
        background-color: rgba(151, 151, 151, 0.2);
        position: absolute;
        top: 50%;
        left: 0px;
        margin-right: -50%;
        transform: translate(-50%, -50%);
      }
    }
  `}
`;
