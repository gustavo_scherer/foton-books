import parse from 'html-react-parser';
import { useNavigate, useParams } from 'react-router-dom';
import { useBookById } from 'useCases/contexts/home';
import { parseAuthor } from 'lib/contexts/book';
import { Icon, Stack } from 'components/structure';
import { bookCoverPlaceholder } from 'constants/images';
import * as S from './Book.styles';
import { BookCoverSkelenton } from './BookCover.skelenton';

export const BookPage = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const { data, isLoading } = useBookById(id);

  return (
    <S.Container>
      <S.Header>
        <S.HeaderBackground />
        <S.Wrapper>
          <S.BackButton onClick={() => navigate(-1)}>
            <Icon icon="IcArrowBack" />
          </S.BackButton>
          <S.ImageContainer>
            {isLoading ? (
              <BookCoverSkelenton />
            ) : (
              <S.BookCover
                src={
                  data?.data.volumeInfo.imageLinks?.thumbnail ??
                  bookCoverPlaceholder
                }
                alt={`Cover of ${data?.data.volumeInfo.title}`}
              />
            )}
          </S.ImageContainer>
        </S.Wrapper>
      </S.Header>
      <S.Content>
        <Stack spacing="10px">
          <S.Title>{data?.data.volumeInfo.title}</S.Title>
          <S.Author>{parseAuthor(data?.data.volumeInfo.authors)}</S.Author>
          <S.Summary>
            {parse(data?.data.volumeInfo.description ?? '')}
          </S.Summary>
        </Stack>
      </S.Content>
      <S.ActionsMenu>
        <S.Option href={data?.data.accessInfo.webReaderLink} target="_blank">
          <Icon icon="IcRead" />
          Read
        </S.Option>
        <S.Option href={data?.data.accessInfo.webReaderLink} target="_blank">
          <Icon icon="IcListen" />
          Listen
        </S.Option>
        <S.Option href={data?.data.volumeInfo.infoLink} target="_blank">
          <Icon icon="IcShare" />
          Share
        </S.Option>
      </S.ActionsMenu>
    </S.Container>
  );
};
