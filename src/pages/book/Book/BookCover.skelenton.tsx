import ContentLoader from 'react-content-loader';

export const BookCoverSkelenton = () => (
  <ContentLoader
    speed={2}
    width={151}
    height={231}
    viewBox="0 0 151 234"
    backgroundColor="#cecece"
    foregroundColor="#ecebeb"
  >
    <rect x="6" y="-1" rx="0" ry="0" width="151" height="234" />
  </ContentLoader>
);
