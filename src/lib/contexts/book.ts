export const parseAuthor = (author: string[] | string | undefined) => {
  if (!author) return '';

  return Array.isArray(author) ? author.join(' and ') : author;
};
