import { Book } from 'interfaces/book';

export const getSubArraysLength = (array: Book[][]) =>
  array.reduce((acc, element) => acc + element.length, 0);
