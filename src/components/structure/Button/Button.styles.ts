import styled from 'styled-components';

export const Container = styled.button`
  text-align: center;
  background: #ff6978;
  box-shadow: 5px 5px 80px rgba(212, 173, 134, 0.4926);
  border-radius: 10px;
  padding: 10px;
  color: #fff;
  font-weight: 600;
  font-size: 16px;
  width: 336px;
  transition: all 0.2s linear;

  &:hover {
    opacity: 0.6;
  }

  &:disabled {
    background: lightgray;
    cursor: not-allowed;
  }
`;
