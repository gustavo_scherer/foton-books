import { ButtonHTMLAttributes, PropsWithChildren } from 'react';
import * as S from './Button.styles';

export type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement>;

export const Button = ({
  children,
  ...props
}: PropsWithChildren<ButtonProps>) => (
  <S.Container {...props}>{children}</S.Container>
);
