import styled, { css } from 'styled-components';

type ContainerProps = {
  spacing?: string;
  direction?: 'vertical' | 'horizontal';
  center?: boolean;
};

const stackModifiers = {
  spacing: (value: string) => css`
    & > * {
      margin-top: ${value};

      &:first-child {
        margin-top: 0;
      }
    }
  `,
  direction: {
    vertical: () => css`
      flex-direction: column;
    `,
    horizontal: () => css`
      flex-direction: row;
    `,
  },
  center: () => css`
    justify-content: center;
  `,
};

export const Container = styled.div<ContainerProps>`
  ${({ direction = 'vertical', spacing = '20px', center = false }) => css`
    display: flex;

    ${direction && stackModifiers.direction[direction]()};
    ${spacing && stackModifiers.spacing(spacing)};
    ${center && stackModifiers.center()};
  `}
`;
