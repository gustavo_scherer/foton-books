import * as React from 'react';
import * as S from './Stack.styles';

export type StackProps = {
  spacing?: string;
  direction?: 'vertical' | 'horizontal';
  center?: boolean;
};

export const Stack = ({
  children,
  spacing,
  direction,
  center,
}: React.PropsWithChildren<StackProps>) => (
  <S.Container spacing={spacing} direction={direction} center={center}>
    {children}
  </S.Container>
);
