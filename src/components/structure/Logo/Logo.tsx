import { Link } from 'react-router-dom';
import { Icon } from 'components/structure';
import { ROUTES } from 'constants/urls';

export const Logo = () => (
  <Link to={ROUTES.app.getLink('home')}>
    <Icon icon="IcLogo" width="200px" height="60px" />
  </Link>
);
