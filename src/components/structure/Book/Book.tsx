import { useNavigate } from 'react-router-dom';
import { bookCoverPlaceholder } from 'constants/images';
import { ROUTES } from 'constants/urls';
import * as S from './Book.styles';

type BooksProps = {
  coverSrc?: string;
  title: string;
  author: string;
  bookId: string;
};

export const Book = ({
  coverSrc = bookCoverPlaceholder,
  title,
  author,
  bookId,
}: BooksProps) => {
  const navigate = useNavigate();

  const handleOnClick = () => {
    navigate(ROUTES.book.getLink('id', bookId));
  };

  return (
    <S.Container onClick={handleOnClick}>
      {coverSrc && (
        <img src={coverSrc} alt={`cover of ${title} by ${author}`} />
      )}
      <S.Info>
        <S.Title>{title}</S.Title>
        <S.Author>by {author}</S.Author>
      </S.Info>
    </S.Container>
  );
};
