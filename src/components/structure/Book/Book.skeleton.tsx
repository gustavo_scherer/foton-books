import ContentLoader from 'react-content-loader';

export const BookSkeleton = () => (
  <ContentLoader
    speed={2}
    width={108}
    height={158}
    viewBox="0 0 108 158"
    backgroundColor="#cecece"
    foregroundColor="#ecebeb"
  >
    <rect x="4" y="2" rx="5" ry="5" width="100" height="150" />
    <rect x="4" y="2" rx="0" ry="0" width="100" height="150" />
  </ContentLoader>
);
