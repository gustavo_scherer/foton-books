import styled, { css } from 'styled-components';
import media from 'styled-media-query';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  cursor: pointer;
  transition: all 0.2s linear;

  &:hover {
    opacity: 0.6;
  }

  & > img {
    width: 100px;
    height: 150px;
  }

  ${media.greaterThan('medium')`
    flex-direction: row; 
    align-items: unset; 

    & > img {
      margin-right: 5px;
      filter: drop-shadow(0px 2px 4px rgba(229, 229, 229, 0.5));
    }
  `}
`;

export const Title = styled.span`
  ${({ theme }) => css`
    margin-top: ${theme.spacing.xs};
    font-size: ${theme.typography.sizes.xs};
    font-weight: 700;
    color: rgba(49, 49, 49, 0.8);
  `}
`;

export const Author = styled.span`
  ${({ theme }) => css`
    margin-top ${theme.spacing['2xs']};
    font-size: ${theme.typography.sizes['2xs']};
    font-weight: 700;
    color: rgba(49, 49, 49, 0.8);
  `};
`;

export const Info = styled.span`
  ${() => css`
    display: flex;
    flex-direction: column;
    max-width: 100px;
  `};
`;
