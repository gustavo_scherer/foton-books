import { useNavigate } from 'react-router-dom';
import { Button } from 'components/structure';
import * as S from './ErrorGeneric.styles';

export type ErrorGenericProps = {
  title: string | JSX.Element;
  subtitle?: string | JSX.Element;
  description: string | JSX.Element;
  buttonLabel: string | JSX.Element;
  actionNavigate?: string;
  actionReload?: () => void;
  isSpaceFull?: boolean;
};

export const ErrorGeneric = ({
  title,
  description,
  buttonLabel,
  actionNavigate,
  actionReload,
}: ErrorGenericProps) => {
  const navigate = useNavigate();

  return (
    <S.Container>
      <S.Content>
        <S.Title>{title}</S.Title>

        <p>{description}</p>
        <Button
          onClick={() => {
            actionReload && actionReload();
            actionNavigate && navigate(actionNavigate);
          }}
        >
          {buttonLabel}
        </Button>
      </S.Content>
    </S.Container>
  );
};
