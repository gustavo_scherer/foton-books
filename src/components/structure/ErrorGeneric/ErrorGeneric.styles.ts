import styled, { css } from 'styled-components';
import media from 'styled-media-query';

type ContainerProps = {
  src?: string;
  isSpaceFull?: boolean;
};

export const Container = styled.div<ContainerProps>`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100vh;
    width: 100%;
    background: ${theme.colors.background.book.header} no-repeat center;
    background-size: cover;
    padding: ${theme.spacing.lg} ${theme.spacing['2xl']};

    ${media.lessThan('large')`
      padding: ${theme.spacing.xl} ${theme.spacing.md} ${theme.spacing.md};

    `}
  `}
`;

export const Content = styled.div`
  ${({ theme }) => css`
    height: 100%;
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;

    p {
      color: ${theme.colors.grayscale.light.paragraph};
      margin-top: ${theme.spacing.md};
      max-width: 418px;
      text-align: center;
      line-height: ${theme.spacing.md};
    }
  `}
`;

export const Title = styled.h1`
  ${({ theme }) => css`
    color: ${theme.colors.secondary.main};
    font-size: ${theme.typography.sizes['4xl']};
    margin-bottom: ${theme.spacing.sm};
    font-weight: normal;
    text-transform: uppercase;

    ${media.lessThan('medium')`
      font-size: 64px;
      line-height: 40px;
    `}
  `}
`;
