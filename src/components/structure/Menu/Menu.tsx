import { Icon } from 'components/structure';
import { menuOptionsMock } from './Menu.mock';
import * as S from './Menu.styles';

export const Menu = () => (
  <S.Container>
    {menuOptionsMock.map(({ label, url, icon, ...rest }, index) => (
      <S.Option
        activeClassName="active"
        to={url}
        key={`${label}-${index}`}
        {...rest}
      >
        <Icon icon={icon} />
        <span>{label}</span>
      </S.Option>
    ))}
  </S.Container>
);
