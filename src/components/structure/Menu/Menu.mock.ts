import { AnchorHTMLAttributes } from 'react';
import { ROUTES } from 'constants/urls';
import * as Icons from 'assets/icons';

type MenuOptionsMockProps = Array<
  {
    label: string;
    url: string;
    icon: keyof typeof Icons;
    disabled?: boolean;
  } & AnchorHTMLAttributes<HTMLAnchorElement>
>;

export const menuOptionsMock: MenuOptionsMockProps = [
  { label: 'Home', url: ROUTES.app.getLink('home'), icon: 'IcHome' },
  { label: 'Libraries', url: '#', disabled: true, icon: 'IcBook' },
  { label: 'Profile', url: '#', disabled: true, icon: 'IcUser' },
];
