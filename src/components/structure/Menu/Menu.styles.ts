import { NavLink } from 'react-router-dom';
import styled, { css } from 'styled-components';
import media from 'styled-media-query';

type OptionProps = {
  disabled?: boolean;
};

export const Container = styled.div`
  ${({ theme }) => css`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    width: 100%;
    height: ${theme.sizes.menu};
    bottom: 0;
    left: 0;
    position: fixed;
    background-color: ${theme.colors.neutral.white};

    ${media.greaterThan('medium')`
      position: relative; 
      background: transparent;
    `}
  `}
`;

export const Option = styled(NavLink)<OptionProps>`
  ${({ theme, disabled = false }) => css`
    display: flex;
    flex-direction: column;
    width: 100%;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    transition: all .2s linear;

    & > svg > path,
    & > svg > circle {
      transition: all .2s linear;
    } 

    &:hover {
      color: ${theme.colors.neutral.black};

      & > svg > path,
      & > svg > circle {
        stroke:  ${theme.colors.neutral.black};
      }
    }

    &.active {
      color: ${theme.colors.neutral.black};

      & > svg > path,
      & > svg > circle {
        stroke:  ${theme.colors.neutral.black};
    }

    ${
      disabled &&
      css`
        color: #bfbebf;
        cursor: not-allowed;

        & > svg > path,
        & > svg > circle {
          stroke: #bfbebf;
        }


        &:hover {
          color: bfbebf;

          & > svg > path,
          & > svg > circle {
            stroke: #bfbebf;
        }
      `
    }
  `}
`;
