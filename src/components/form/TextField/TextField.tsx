import { InputHTMLAttributes, forwardRef } from 'react';
import { Icon } from 'components/structure';
import * as S from './TextField.styles';

export type Mask = 'hour' | 'date';

export type TextFieldProps = {
  label?: string;
  type?: string;
  icon?: React.ReactElement;
  hasIcon?: boolean;
} & InputHTMLAttributes<HTMLInputElement>;

export const TextField = forwardRef<HTMLInputElement, TextFieldProps>(
  ({ label, name, type, hasIcon = false, ...props }, ref) => (
    <S.Container>
      <S.InputWrapper>
        {hasIcon && (
          <S.IconContainer>
            <Icon icon="IcSearch" />
          </S.IconContainer>
        )}
        <S.Field type={type} ref={ref} id={name} name={name} {...props} />
        {!!label && (
          <S.Label htmlFor={name} hasIcon={hasIcon}>
            {label}
          </S.Label>
        )}
      </S.InputWrapper>
    </S.Container>
  ),
);
