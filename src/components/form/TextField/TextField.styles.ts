import styled, { css, DefaultTheme } from 'styled-components';
import { TextFieldProps } from './TextField';

type FieldProps = {
  hasError?: boolean;
  success?: boolean;
  newPassword?: boolean;
};

type LabelProps = Pick<TextFieldProps, 'hasIcon'>;

const fieldModifiers = {
  hasError: (theme: DefaultTheme) => css`
    &,
    &:active,
    &:focus {
      border-color: ${theme.colors.contextual.danger.main};
    }

    & + ${Label} {
      color: ${theme.colors.contextual.danger.main};
    }
  `,
  hasIcon: () => css`
    left: 54px;
  `,
};

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  box-shadow: 5px 5px 80px rgba(212, 173, 134, 0.122623);
`;

export const InputWrapper = styled.div<FieldProps>`
  ${({ theme, hasError }) => css`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    background: ${theme.colors.background.light.input};
    font-family: ${theme.typography.family.primary};
    font-weight: 500;
    border-radius: 10px;
    position: relative;
    width: 100%;
    text-align: left;
    height: ${theme.sizes.input};
    min-height: ${theme.sizes.input};
    outline: none;
    transition: all 0.2s linear;
    transition-property: color, border-color;
    text-overflow: ellipsis;
    white-space: nowrap;

    &:active,
    &:focus-within {
      border-color: ${theme.colors.primary.main};

      & > ${Label} {
        color: ${theme.colors.primary.main};
      }
    }

    &:disabled {
      background-color: ${theme.colors.grayscale.light.line};
      pointer-events: none;
    }

    ${hasError && fieldModifiers.hasError(theme)};
  `}
`;

export const Field = styled.input<FieldProps>`
  ${({ theme }) => css`
    appearance: none;
    background: none;
    border: none;
    font-family: inherit;
    font-size: ${theme.typography.sizes.base};
    line-height: ${theme.typography.lineHeight.base};
    color: ${theme.colors.neutral.black};
    font-weight: 600;
    height: 48px;
    min-height: 48px;
    overflow: hidden;
    outline: none;
    padding: 10px 10px 0 10px;
    text-overflow: ellipsis;
    white-space: nowrap;
    width: 100%;

    &::placeholder {
      font-family: inherit;
      color: #54565a;
      font-weight: normal;
    }

    &:placeholder-shown {
      padding: ${theme.spacing.sm} 20px;

      & + ${Label} {
        top: 58%;
        opacity: 0;
        visibility: hidden;
      }
    }
  `}
`;

export const Label = styled.label<LabelProps>`
  ${({ theme, hasIcon = false }) => css`
    color: ${theme.colors.primary.main};
    font-size: 10px;
    font-family: inherit;
    transition: all 0.2s linear;
    position: absolute;
    left: 10px;
    top: 10px;
    transform: translateY(-50%);
    ${hasIcon && fieldModifiers.hasIcon()}
  `}
`;

export const IconContainer = styled.div`
  ${() => css`
    display: flex;
    align-items: center;
    justify-content: center;
    margin-left: 20px;
  `}
`;
