import styled, { css } from 'styled-components';
import media from 'styled-media-query';

export const Container = styled.div`
  ${({ theme }) => css`
    display: block;
    width: 100%;
    height: 100vh;
    background-color: ${theme.colors.background.outlet.default};
    overflow: hidden;
  `}
`;

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`;

export const Grid = styled.div`
  margin: 0 auto;
  display: grid;
  grid-template-areas:
    'searchbar'
    'content'
    'menu';
  max-width: 1120px;
  grid-template-columns: 1fr;
  grid-template-rows: 128px 1fr 59px;
  width: 100%;
  height: 100%;
  position: relative;
  grid-column-gap: 20px;

  ${media.greaterThan('medium')`
    grid-template-areas:
      'logo searchbar menu'
      'space content space2';
    grid-template-columns: 1fr 640px 1fr;
    grid-template-rows: 128px 1fr;
    padding: 0;
  `};
`;

export const LogoWrapper = styled.section`
  display: none;

  ${media.greaterThan('medium')`
    display: flex;
    grid-area: logo;
    width: 100%;
    height: 100%;
    align-items: center;;
  `}
`;

export const SearchBar = styled.section`
  grid-area: searchbar;
  width: 100%;
  height: 100%;
  padding: 40px 20px 0;
`;

export const Menu = styled.section`
  grid-area: menu;
  width: 100%;
  height: 100%;
  display: block;

  ${media.greaterThan('medium')`
    display: flex;
    justify-content: flex-end;
    align-items: center;
  `}
`;

export const Content = styled.section`
  grid-area: content;
  width: 100%;
  height: 100%;
  overflow-y: scroll;
  padding: 0 20px;
  padding-bottom: 10px;

  ::-webkit-scrollbar {
    width: 5px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: #f1f1f1;
    border-radius: 10px;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: #888;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #555;
  }
`;
