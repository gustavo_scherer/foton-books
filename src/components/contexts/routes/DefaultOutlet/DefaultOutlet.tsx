import { useEffect } from 'react';
import { Outlet, useNavigate } from 'react-router-dom';
import { useDebounce } from 'hooks/useDebouce';
import { useQueryParams } from 'hooks/useQueryParams';
import { useSearchContext } from 'contexts/search';
import { TextField } from 'components/form';
import { Menu, Logo } from 'components/structure';
import { ROUTES } from 'constants/urls';
import * as S from './DefaultOutlet.styles';

export function DefaultOutlet() {
  const navigate = useNavigate();
  const query = useQueryParams();
  const { setSearch } = useSearchContext();

  const handleFilterByName = useDebounce((value: string) => {
    if (value.length === 0) {
      setSearch('');
      navigate(ROUTES.app.getLink('home'));
      return;
    }

    setSearch(value);
    navigate(`${ROUTES.app.getLink('search')}?q=${encodeURI(value)}`);
  }, 1000);

  useEffect(() => {
    const queryParam = query.get('q');

    if (queryParam) {
      setSearch(queryParam);
    }
  }, [query, setSearch]);

  return (
    <S.Container>
      <S.Wrapper>
        <S.Grid>
          <S.LogoWrapper>
            <Logo />
          </S.LogoWrapper>
          <S.SearchBar>
            <TextField
              label="Search book"
              placeholder="Search book"
              onChange={({ target: { value } }) => handleFilterByName(value)}
              hasIcon
            />
          </S.SearchBar>
          <S.Menu>
            <Menu />
          </S.Menu>
          <S.Content>
            <Outlet />
          </S.Content>
        </S.Grid>
      </S.Wrapper>
    </S.Container>
  );
}
