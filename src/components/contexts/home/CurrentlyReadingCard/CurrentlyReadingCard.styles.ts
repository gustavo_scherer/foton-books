import styled, { css } from 'styled-components';
import media from 'styled-media-query';
import { Icon } from 'components/structure';
import theme from 'styles/theme';
import currentlyReadingDetails from 'assets/icons/core/currently-reading-details.svg';

export const Container = styled.div`
  ${({ theme }) => css`
    width: 331px;
    height: 100px;
    background-image: url(${currentlyReadingDetails});
    background-repeat: no-repeat;
    background-position: -5%;
    background-color: ${theme.colors.quaternary.main};
    border-radius: 5px;
    position: relative;
    margin-top: 20px;
    display: flex;
    padding: 10px;
    justify-content: center;
    transition: all 0.2s linear;
    cursor: pointer;

    &:hover {
      opacity: 0.6;
    }

    ${media.lessThan('medium')`
    border-radius: 0 5px 5px 0; 
    margin-left: -20px;
  `}
  `}
`;

export const BookCover = styled.img`
  position: absolute;
  width: 91px;
  height: 136px;
  left: 20px;
  top: -20px;
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const TitleAuthorWrapper = styled.div``;

export const Title = styled.h1`
  ${({ theme }) => css`
    font-family: ${theme.typography.family.bookTitle};
    font-size: 20px;
  `};
`;

export const Author = styled.h2`
  font-family: ${theme.typography.family.author};
  font-size: 10px;
  font-weight: 400;
  color: #74776d;
`;

export const Chapters = styled.span`
  font-size: 10px;
  display: flex;
  align-items: center;

  & > ${Icon} {
    margin-right: 2px;
  }

  & > strong {
    color: #ff6978;
    margin: 0 3px;
  }
`;

export const SkeletonContainer = styled.div`
  ${() => css`
    display: flex;
    height: 110px;
    max-width: 300px;
    border-radius: 5px;
    cursor: pointer;
  `}
`;
