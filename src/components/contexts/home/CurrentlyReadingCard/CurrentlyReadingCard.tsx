import { useNavigate } from 'react-router-dom';
import { parseAuthor } from 'lib/contexts/book';
import { Icon } from 'components/structure';
import { Book } from 'interfaces/book';
import { bookCoverPlaceholder } from 'constants/images';
import { ROUTES } from 'constants/urls';
import { CurrentlyReadingSkeleton } from './CurrentlyReadingCard.skeleton';
import * as S from './CurrentlyReadingCard.styles';

type CurrentlyReadingCardProps = {
  isLoading?: boolean;
  book?: Book;
};

export const CurrentlyReadingCard = ({
  book,
  isLoading = false,
}: CurrentlyReadingCardProps) => {
  const navigate = useNavigate();

  const handleOnClick = () => {
    navigate(ROUTES.book.getLink('id', book?.id));
  };

  if (isLoading)
    return (
      <S.SkeletonContainer>
        <CurrentlyReadingSkeleton />
      </S.SkeletonContainer>
    );

  return (
    <S.Container onClick={handleOnClick}>
      <S.BookCover
        src={
          book?.volumeInfo.imageLinks?.smallThumbnail ?? bookCoverPlaceholder
        }
      />
      <S.Info>
        <S.TitleAuthorWrapper>
          <S.Title>{book?.volumeInfo.title ?? ''}</S.Title>
          <S.Author>by {parseAuthor(book?.volumeInfo.authors ?? '')}</S.Author>
        </S.TitleAuthorWrapper>
        <S.Chapters>
          <Icon icon="IcChapter" width="16px" height="16px" />
          Chapter
          <strong>2</strong> From 9
        </S.Chapters>
      </S.Info>
    </S.Container>
  );
};
