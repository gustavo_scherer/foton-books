import ContentLoader from 'react-content-loader';

export const DiscoverCardSkeleton = () => (
  <ContentLoader
    speed={2}
    width="100%"
    height="100%"
    backgroundColor="#cecece"
    foregroundColor="#ecebeb"
  >
    <rect x="0" y="0" rx="5" ry="5" width="100%" height="139" />
    <rect x="0" y="0" rx="0" ry="0" width="100%" height="139" />
  </ContentLoader>
);
