import styled, { css, DefaultTheme } from 'styled-components';
import { Icon } from 'components/structure';
import ovalUrl from 'assets/icons/core/oval.svg';

type ContainerProps = {
  backgroundPosition?: 'top' | 'bottom';
  isPurple?: boolean;
};

const containerModifiers = {
  backgroundPosition: {
    top: () => css`
      background-image: url(${ovalUrl});
      background-repeat: no-repeat;
      background-position: -5% 170%;
    `,
    bottom: () => css`
      background-image: url(${ovalUrl});
      background-repeat: no-repeat;
      background-position: -5% -170%;
    `,
  },
  isPurple: (theme: DefaultTheme) => css`
    background-color: ${theme.colors.tertiary.main};
  `,
};

export const Container = styled.div<ContainerProps>`
  ${({ theme, backgroundPosition = 'bottom', isPurple = false }) => css`
    display: flex;
    justify-content: space-between;
    height: 139px;
    background-color: ${theme.colors.secondary.main};
    border-radius: 5px;
    margin: 15px;
    padding: 15px 24px;
    color: #fefefe;
    cursor: pointer;
    transition: opacity 0.2s linear;

    &:hover {
      opacity: 0.6;
    }

    ${backgroundPosition &&
    containerModifiers.backgroundPosition[backgroundPosition]()};
    ${isPurple && containerModifiers.isPurple(theme)};
  `}
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const TitleAuthorWrapper = styled.div``;

export const Title = styled.h1`
  ${({ theme }) => css`
    font-family: ${theme.typography.family.bookTitle};
    font-size: 27px;
  `};
`;

export const Author = styled.h2`
  font-size: 14px;
  font-style: italic;
  font-weight: 400;
`;

export const Readings = styled.span`
  font-size: 10px;
  display: flex;
  align-items: center;

  & > ${Icon} {
    margin-right: 5px;
  }

  & > strong {
    margin-right: 5px;
  }
`;

export const BookWrapper = styled.div`
  position: relative;

  & > ${Icon} {
    position: absolute;
    top: -12px;
    left: -35px;
  }
`;

export const BookCover = styled.img`
  width: 73px;
  height: 109px;
`;

export const SkeletonContainer = styled.div`
  ${() => css`
    display: flex;
    height: 139px;
    border-radius: 5px;
    margin: 15px;
    cursor: pointer;
  `}
`;
