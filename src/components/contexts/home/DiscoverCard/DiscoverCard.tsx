import { useNavigate } from 'react-router-dom';
import { parseAuthor } from 'lib/contexts/book';
import { Icon } from 'components/structure';
import { Book } from 'interfaces/book';
import { bookCoverPlaceholder } from 'constants/images';
import { ROUTES } from 'constants/urls';
import { DiscoverCardSkeleton } from './DiscoverCard.skeleton';
import * as S from './DiscoverCard.styles';

type DiscoverCardProps = {
  book?: Book;
  backgroundPosition?: 'top' | 'bottom';
  isPurple?: boolean;
  isLoading?: boolean;
};

export const DiscoverCard = ({
  backgroundPosition,
  isPurple,
  book,
  isLoading = false,
}: DiscoverCardProps) => {
  const navigate = useNavigate();

  const handleOnClick = () => {
    navigate(ROUTES.book.getLink('id', book?.id));
  };

  if (isLoading)
    return (
      <S.SkeletonContainer>
        <DiscoverCardSkeleton />
      </S.SkeletonContainer>
    );

  return (
    <S.Container
      backgroundPosition={backgroundPosition}
      isPurple={isPurple}
      onClick={handleOnClick}
    >
      <S.Info>
        <S.TitleAuthorWrapper>
          <S.Title>{book?.volumeInfo.title ?? ''}</S.Title>
          <S.Author>{parseAuthor(book?.volumeInfo.authors) ?? ''}</S.Author>
        </S.TitleAuthorWrapper>
        <S.Readings>
          <Icon icon="IcReadings" width="16px" height="16px" />
          <strong>{book?.volumeInfo.pageCount ?? ''}+</strong> Read Now
        </S.Readings>
      </S.Info>
      <S.BookWrapper>
        <S.BookCover
          src={
            book?.volumeInfo.imageLinks?.smallThumbnail ?? bookCoverPlaceholder
          }
        />
        <Icon icon="IcBookDetails" width="57px" height="106px" />
      </S.BookWrapper>
    </S.Container>
  );
};
