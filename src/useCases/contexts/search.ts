import { useInfiniteQuery } from 'react-query';
import { useAPI } from 'hooks/useAPI';
import { getSubArraysLength } from 'lib/contexts/search';
import { useSearchContext } from 'contexts/search';
import { Book } from 'interfaces/book';
import { numberOfBooksPerPage } from 'constants/books';
import { VOLUMES } from 'constants/endpoints';

export const useSearchResult = () => {
  const api = useAPI();
  const { search } = useSearchContext();

  return useInfiniteQuery<Book[]>(
    ['books', search],
    async ({ pageParam = 0 }) => {
      const response = await api.get(
        VOLUMES.SEARCH(search ?? '', numberOfBooksPerPage, pageParam),
      );

      return response.data.items ?? [];
    },
    {
      getNextPageParam: (lastPage, pages) => {
        if (lastPage.length === 0) {
          return false;
        }

        const numberOfBooksFetch = getSubArraysLength(pages);
        const currentBookIndex = numberOfBooksFetch + numberOfBooksPerPage;

        return currentBookIndex;
      },
    },
  );
};
