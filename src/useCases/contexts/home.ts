import { useQuery } from 'react-query';
import { useAPI } from 'hooks/useAPI';
import { useQueriesTyped } from 'hooks/useQueriesTyped';
import { Book } from 'interfaces/book';
import { VOLUMES } from 'constants/endpoints';

export const useBooksByIds = (bookIds: string[]) => {
  const api = useAPI();

  return useQueriesTyped(
    bookIds.map((id) => ({
      queryKey: ['book', id],
      queryFn: () => api.get<Book>(VOLUMES.BY_ID(id)),
    })),
  );
};

export const useBookById = (id: string) => {
  const api = useAPI();

  return useQuery(['book', id], () => api.get<Book>(VOLUMES.BY_ID(id)));
};
