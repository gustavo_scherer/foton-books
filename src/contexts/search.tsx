import { createContext, PropsWithChildren, useContext, useState } from 'react';

type ContextType = {
  search?: string;
  setSearch: (search: string) => void;
};

const defaultValue: ContextType = {
  search: undefined,
  setSearch: () => undefined,
};

export const SearchContext = createContext<ContextType>(defaultValue);

type ServiceProviderProps = unknown;

export const SearchProvider = ({
  children,
}: PropsWithChildren<ServiceProviderProps>) => {
  const [search, setSearch] = useState(defaultValue.search);

  return (
    <SearchContext.Provider
      value={{
        search,
        setSearch,
      }}
    >
      {children}
    </SearchContext.Provider>
  );
};

export const useSearchContext = () => {
  const context = useContext(SearchContext);

  if (!context) {
    throw new Error(`useSearchContext must be used within an AuthProvider`);
  }

  return context;
};
