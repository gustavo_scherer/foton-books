import {
  useQueries as useQueriesBase,
  UseQueryOptions,
  QueryObserverResult,
} from 'react-query';

export const useQueriesTyped = <
  TQueryFnData = unknown,
  TError = unknown,
  TData = TQueryFnData,
>(
  queries: UseQueryOptions<TQueryFnData, TError, TData>[],
) =>
  useQueriesBase(
    queries as UseQueryOptions<unknown, unknown, unknown>[],
  ) as QueryObserverResult<TQueryFnData, TError>[];
