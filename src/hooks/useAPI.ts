import axios from 'axios';
import { useRef } from 'react';
import { baseURL } from 'constants/endpoints';

export const useAPI = () => {
  const apiRef = useRef(
    axios.create({
      baseURL,
      headers: {
        'Content-Type': 'application/json',
      },
    }),
  );

  return apiRef.current;
};
