export type Book = {
  id: string;
  accessInfo: {
    webReaderLink: string;
  };
  volumeInfo: {
    pageCount: number;
    infoLink: string;
    title: string;
    imageLinks?: {
      smallThumbnail: string;
      thumbnail: string;
    };
    authors?: string[];
    description: string;
  };
};

export type FetchBooksResponse = {
  items: Book[];
};
