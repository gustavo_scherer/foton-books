export const colors = {
  primary: {
    main: '#FF6978',
  },
  secondary: {
    main: '#00173D',
  },
  tertiary: {
    main: '#451475',
  },
  quaternary: {
    main: '#EEF5DB',
  },
  link: {
    main: '#4ABDF1',
  },
  neutral: {
    black: '#000000',
    filter: 'rgba(0, 0, 0, 0.6)',
    white: '#ffffff',
  },
  grayscale: {
    light: {
      title: '#36383A',
      name: '#54565A',
      section: '#3F4043',
      paragraph: 'rgba(49, 49, 49, 0.6)',
      info: 'rgba(0, 0, 0, 0.5)',
      line: 'rgba(0, 0, 0, 0.2)',
    },
  },
  background: {
    light: {
      input: '#FDFCFC',
      button: '#FF6978',
    },
    outlet: {
      default: '#F2F2F2',
    },
    menu: {
      main: '#FFFFFF',
    },
    book: {
      header: '#FFF6E5',
    },
  },
  contextual: {
    info: {
      main: '',
      hover: '',
      active: '',
    },
    success: {
      main: '#38cd3c',
      hover: '#2fc533',
      active: '#22b825',
    },
    attention: {
      main: '#fda700',
      hover: '',
      active: '',
    },
    danger: {
      main: '#ff0202',
      hover: '#cc0202',
      active: '#990303',
    },
    disabled: {
      main: '#BFBEBF',
    },
  },
};
