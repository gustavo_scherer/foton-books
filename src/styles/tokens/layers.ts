export const layers = {
  base: 10,
  menu: 20,
  overlay: 30,
  alwaysOnTop: 50,
  aboveTheTop: 60,
} as const;
