export const shapes = {
  borderRadius: {
    xs: '4px',
    sm: '8px',
    md: '16px',
    lg: '24px',
    full: '999999px',
  },
} as const;
