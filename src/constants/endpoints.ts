export const baseURL = 'https://www.googleapis.com/books/v1/';

export const VOLUMES = {
  SEARCH: (query: string, maxResults = 18, startIndex = 0) =>
    `/volumes?q=${encodeURI(
      query,
    )}&orderBy=relevance&maxResults=${maxResults}&startIndex=${startIndex}`,
  BY_ID: (id: string) => `/volumes/${id}`,
};
