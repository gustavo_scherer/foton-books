import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { BookPage } from 'pages/book';
import { NotFound } from 'pages/error';
import { Home } from 'pages/home';
import { Search } from 'pages/search';
import { SignIn } from 'pages/signIn';
import { useAuthContext } from 'contexts';
import { DefaultOutlet } from 'components/contexts/routes';
import { ROUTES } from 'constants/urls';

const { app, book, signIn } = ROUTES;

export interface RouteProps {
  children?: React.ReactNode;
  element?: React.ReactElement | null;
  path?: string;
}

const PrivateRoute = ({ path, element, children, ...props }: RouteProps) => {
  const { signed } = useAuthContext();

  if (!signed) {
    return <Navigate to={signIn.getLink('root')} />;
  }

  return (
    <Route path={path} element={element} {...props}>
      {children}
    </Route>
  );
};

export const AppRoutes = () => (
  <BrowserRouter>
    <Routes>
      <PrivateRoute>
        <Route path={app.base} element={<DefaultOutlet />}>
          <Route
            path={app.base}
            element={<Navigate to={app.children.home} />}
          />
          <Route path={app.children.home} element={<Home />} />
          <Route path={app.children.search} element={<Search />} />
        </Route>
        <Route path={book.base}>
          <Route path={book.children.id} element={<BookPage />} />
        </Route>
      </PrivateRoute>
      <Route path={signIn.base}>
        <Route path={signIn.children.root} element={<SignIn />} />
      </Route>
      <Route path="*" element={<NotFound />} />
    </Routes>
  </BrowserRouter>
);

export default AppRoutes;
