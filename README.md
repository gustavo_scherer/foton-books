Foton Books 📚
==============

> Foton books test, a project to display books in a elegant way

## Test in Vercel


[Clique aqui para testar app no vercel](https://foton-books-gules.vercel.app)

## Prerequisites

- npm >=5.5.0
- node >=9.3.0

## Install

```sh
npm install
```

or with yarn

```sh
yarn
```

## Usage

```sh
npm run start
```

or with yarn

```sh
yarn start
```
## 🚀  Technologies

- [ReactJS](https://reactjs.org/)
- [react-router-dom](https://github.com/ReactTraining/react-router)
- [styled-components](https://www.styled-components.com/)
- [styled-media-query](https://github.com/morajabi/styled-media-query)
- [axios](https://github.com/axios/axios)
- [React-Query](https://react-query.tanstack.com/)
- [react-content-loader](https://github.com/danilowoz/react-content-loader)
- [react-slick](https://github.com/akiran/react-slick)

## Author

👤 **Gustavo Gualbano** [Get in touch!](https://www.linkedin.com/in/gustavo-gualbano-378074130//)


## Show your support

Give a ⭐️ if this project helped you!